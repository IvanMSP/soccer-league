#Django Libraries
from django.db import models
from django.contrib.auth.models import AbstractUser

#reusable app
from reusable.constants import *

#Pambolero
from league.models import League, Team
from .choices import GENDER_CHOICES


class User(AbstractUser):
    is_admin_league = models.BooleanField(default=False)
    is_admin_team = models.BooleanField(default=False)
    is_user_app = models.BooleanField(default=False)
    is_referee = models.BooleanField(default=False)
    photo_profile = models.ImageField(
        upload_to='photo_profile',
        default='/profile/default.png',
        **NULL
    )
    genre = models.CharField(
        max_length=100,
        choices=GENDER_CHOICES,
        **NULL
    )

    class Meta:
        db_table = 'auth_user'


class AdminLeagueProfile(models.Model):
    user = models.OneToOneField(
        User,
        related_name="adminleagueprofile",
        on_delete=models.CASCADE,
    )
    league = models.OneToOneField(
        League,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return '{} - {}'.format(self.user.username, self.league.name)


class AdminTeamProfile(models.Model):
    user = models.OneToOneField(
        User,
        related_name="adminteamprofile",
        on_delete=models.CASCADE
    )
    team = models.OneToOneField(
        Team,
        on_delete=models.CASCADE
    )


class RefereeProfile(models.Model):
    user = models.OneToOneField(
        User,
        related_name="refereeprofile",
        on_delete=models.CASCADE
    )
    league = models.ForeignKey(
        League,
        related_name='referees',
        on_delete=models.CASCADE
    )
    dob = models.DateField()
    stature = models.CharField(max_length=10, **NULL)
    weight = models.CharField(max_length=10, **NULL)
    nationality = models.CharField(max_length=25, **NULL)

    class Meta:
        verbose_name = 'Arbitro',
        verbose_name_plural = "Arbitros"

    def __str__(self):
        return self.user.username

    def get_full_name(self):
        return '{} {}'.format(self.user.username, self.last_name)

    def get_age(self):
        if self.dob:
            today = date.today()
            return today.year-self.dob.year - ((today.month, today.day) < (self.dob.month, self.dob.day))
        else:
            return None


class UserApp(models.Model):
    user = models.OneToOneField(
        User,
        related_name="userapp",
        on_delete=models.CASCADE
    )